using System;
using System.Collections.Generic;
using Cassandra;

namespace Cassy
{
    class Program
    {

        public static void ResetDb(ISession session)
        {
            session.Execute("Drop table client;");
            session.Execute("Drop table card;");
            session.Execute("Drop table deal;");
            session.Execute("CREATE TABLE client(client_username text ,client_name text,client_password text, client_email text,   PRIMARY KEY(client_username));");
            session.Execute("CREATE TABLE card(card_id text, card_name text, card_price int, card_quantity int, seller_id text, PRIMARY KEY(card_id, seller_id));");
            session.Execute("CREATE TABLE deal(seller_id text, buyer_id text, card_id text, card_name text, deal_amount int, deal_date text, PRIMARY KEY(card_id, seller_id, buyer_id, deal_date));");
        }

        static void Main()
        {
            Cluster cluster = Cluster.Builder().AddContactPoint("localhost").WithPort(9042).WithDefaultKeyspace("vanguard").Build();
            ISession session = cluster.ConnectAndCreateDefaultKeyspaceIfNotExists();

            ResetDb(session);
            MainMenu(session);
            Console.ReadKey();

        }

        private static void MainMenu(ISession session)
        {
            Console.Clear();
            c1:
            Console.WriteLine("1: Login\n2: Register\n3: Quit");
            var key = Console.ReadKey();

            if (key.KeyChar == '1')
            {
                Login(session);
                goto c1;
            }

            if (key.KeyChar == '2')
            {
                Register(session);
                goto  c1;
            }

            if (key.KeyChar == '3')
            {
                Console.WriteLine("Goodbey. Press ENTER.");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Invalid choice");
                goto c1;
            }
        }

        private static void Login(ISession session)
        {
            Console.Clear();
            Console.Write("Username: ");
            var name = Console.ReadLine();
            Console.Write("Password: ");
            var pass = Console.ReadLine();
            RowSet rowSet = null;

            if (name != String.Empty  && pass != String.Empty) rowSet = session.Execute("Select * from client where client_username='" + name + "';");
            else MainMenu(session);

            if (rowSet != null)
            {
                foreach (var row in rowSet)
                {
                    if (pass == row.GetValue<string>("client_password")) Menu(session, name);
                }
            }
            else
            {
                Console.WriteLine("User not found. Press ENTER");
                Console.ReadKey();
                MainMenu(session);
            }
        }

        private static void Register(ISession session)
        {
            Console.Clear();
            Console.Write("Username: ");
            var uname = Console.ReadLine();
            Console.Write("Password: ");
            var pass = Console.ReadLine();
            Console.Write("Full name: ");
            var name = Console.ReadLine();
            Console.Write("email: ");
            var email = Console.ReadLine();

            var ps = session.Prepare(
                "insert into client(client_username ,client_name, client_password , client_email) values (?, ?, ?, ?) if not exists");
            
            var statement = ps.Bind(uname, name, pass, email);
            var rowSet = session.Execute(statement);
            foreach (var row in rowSet)
            {
                if (row[0].ToString() == "True") { Menu(session, uname);}
                else
                {
                    Console.WriteLine("User already exists. Press ENTER.");
                    Console.ReadKey();
                    Register(session);
                }
            }



        }

        private static void Menu(ISession session, string username)
        {

            c1:
            Console.Clear();
            Console.WriteLine("1: Buy\n2: Sell\n3: My deals\n4: Log Out");
            var key = Console.Read();
            if (key == '1')
            {
                Buy(session, username);
                goto c1;
            }

            if (key  == '2')
            {
                Console.Clear();
                Console.ReadLine();
                Console.Write("Card ID: ");
                var cid = Console.ReadLine();
                Console.Write("Card Name: ");
                var cname = Console.ReadLine();
                Console.Write("Card Price: ");
                var cprice = Console.ReadLine();
                Console.Write("Ammount: ");
                var camm = Console.ReadLine();

                var ps = session.Prepare(
                    "insert into card(card_id, card_name, card_price, card_quantity, seller_id) values (?, ?, ?, ?, ?) if not exists;");

                var s = ps.Bind(cid, cname, Convert.ToInt32(cprice), Convert.ToInt32(camm), username);
                var rowSet = session.Execute(s);
                foreach (var row in rowSet)
                {
                    if (row[0].ToString() == "True") {goto c1;}

                    var  rowSetas = session.Execute(session
                        .Prepare("select card_quantity from card where card_id = ? and seller_id = ?")
                        .Bind(cid, username));
                    foreach (var x in rowSetas)
                    {
                        Console.WriteLine("You are already listing this card. Updating stock ammount. Press ENTER.");
                        int namm = Convert.ToInt32(camm) + (int) x[0];
                        //Console.WriteLine(namm);
                        session.Execute(session.Prepare("Update card set card_quantity = ? where card_id = ? and seller_id = ?").Bind(namm, cid, username));
                        Console.ReadKey();
                    }
                }

                goto c1;
            }

            if (key == '3')
            {
                Deals(session, username);
                goto c1;
            }

            if (key == '4')
            {
                Console.WriteLine("Goodbey.");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Invalid choice");
                goto c1;
            }
        }

        private static void Buy(ISession session, string username)
        {
            Console.ReadLine();
            Console.Write("Card ID: ");
            var cid = Console.ReadLine();
            var rowSet = session.Execute(session.Prepare("select * from card where card_id = ?")
                .Bind(cid));
            List<Card> cards = new List<Card>();
            foreach (var row in rowSet)
            {
                cards.Add(new Card(row));
            }

            if (cards.Count > 0)
            {
                Console.WriteLine("These are the offers for this card:");
                Console.WriteLine("No." + "Card Name" + "\t" + "Card Price" + " eu \t" + "Seller's stock" + "\t" + "Seller\n");
                for (int i = 0; i < cards.Count; i++)
                {
                    Console.WriteLine((i+1) + ". " + cards[i].name + "\t" + cards[i].price + " eu \t" + cards[i].ammount + "\t" + cards[i].seller_username);
                }
                Console.WriteLine("\n");
                c1:
                Console.WriteLine("Please provide offer id to make an order ( 0 will cancel ordering): ");
                var select = Console.ReadLine();
                var sId = Convert.ToInt32(select);
                if (sId > 0 && sId <= cards.Count)
                {
                    sId--;
                    c2:
                    Console.WriteLine("Please provide ammount ( 0 will cancel ordering): ");
                    var samm = Convert.ToInt32(Console.ReadLine());
                    if (samm > 0 && samm <= cards[sId].ammount)
                    {
                        var ps = session.Prepare(
                            "insert into deal(seller_id, buyer_id, card_id, card_name, deal_amount, deal_date) values (?, ?, ?, ?, ?, ?) if not exists;");

                        var s = ps.Bind(cards[sId].seller_username, username, cards[sId].id, cards[sId].name, cards[sId].price * samm, DateTime.Today);
                        var rowSet1 = session.Execute(s);
                        foreach (var row in rowSet1)
                        {
                            if (row[0].ToString() == "True")
                            {
                                int namm = cards[sId].ammount - samm;
                                if (namm == 0)
                                {
                                    session.Execute(session.Prepare("Delete from card where card_id = ? and seller_id = ?").Bind(cid, cards[sId].seller_username));
                                }
                                else
                                {
                                    session.Execute(session.Prepare("Update card set card_quantity = ? where card_id = ? and seller_id = ?").Bind(namm, cid, cards[sId].seller_username));
                                }
                            }
                            else
                            {
                                Console.WriteLine("You already placed an order from this seller today. Press Enter.");
                                Console.ReadKey();
                                return;
                            }
                        }



                    }
                    else if(sId == 0)
                    {
                        Console.WriteLine("Canceled ordering. Press enter.");
                    }
                    else
                    {
                        Console.WriteLine("Invalid ammount. Press enter.");
                        Console.ReadKey();
                        goto c2;
                    }

                }
                else if (sId == 0)
                {
                    Console.WriteLine("Canceled ordering. Press enter.");
                }
                else
                {
                    Console.WriteLine("Invalid id.");
                    goto c1;
                }


            }
            else
            {
                Console.WriteLine("We were unable to find any offers for this card. Press ENTER to go back.");
            }
        }
        private static void Deals(ISession session, string username)
        {
            Console.WriteLine("We have sent your orders to email.");
        }

    }
}
