﻿using System;
using System.Runtime.InteropServices;
using Cassandra;

namespace Cassy
{
    public struct Card
    {
        public string id;
        public string name;
        public int price;
        public int ammount;
        public string seller_username;

        public Card(Row row)
        {
            id = row[0].ToString();
            name = row[2].ToString();
            var value = row[3];
            price = Convert.ToInt32(value);
            ammount = Convert.ToInt32(row[4].ToString());
            seller_username = row[1].ToString();
        }
    }
}